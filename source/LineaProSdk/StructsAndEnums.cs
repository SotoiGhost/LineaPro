﻿using System;

namespace LineaProSdk
{
	public enum SupportedDeviceTypes
	{
		Linea = 0,
		Printer,
		Pinpad,
		Iserial,
		PrinterZpl
	}

	public enum EmvPinEntry
	{
		Automatic = 0,
		Enabled,
		Disabled
	}

	public enum Barcodes
	{
		All = 0,
		Upc,
		Codabar,
		Code25Ni2of5,
		Code25I2of5,
		Code39,
		Code93,
		Code128,
		Code11,
		Cpcbinary,
		Dun14,
		Ean2,
		Ean5,
		Ean8,
		Ean13,
		Ean128,
		Gs1databar,
		Itf14,
		LatentImage,
		Pharmacode,
		Planet,
		Postnet,
		IntelligentMail,
		Msi,
		Postbar,
		Rm4scc,
		Telepen,
		Plessey,
		Pdf417,
		Micropdf417,
		Datamatrix,
		Aztek,
		Qrcode,
		Maxicode,
		Last
	}

	public enum BarcodesEx
	{
		All = 0,
		Upca,
		Codabar,
		Code25Ni2of5,
		Code25I2of5,
		Code39,
		Code93,
		Code128,
		Code11,
		Cpcbinary,
		Dun14,
		Ean2,
		Ean5,
		Ean8,
		Ean13,
		Ean128,
		Gs1databar,
		Itf14,
		LatentImage,
		Pharmacode,
		Planet,
		Postnet,
		IntelligentMail,
		MsiPlessey,
		Postbar,
		Rm4scc,
		Telepen,
		UkPlessey,
		Pdf417,
		Micropdf417,
		Datamatrix,
		Aztek,
		Qrcode,
		Maxicode,
		Reserved1,
		Reserved2,
		Reserved3,
		Reserved4,
		Reserved5,
		Upca2,
		Upca5,
		Upce,
		Upce2,
		Upce5,
		Ean132,
		Ean135,
		Ean82,
		Ean85,
		Code39Full,
		ItaPharma,
		CodabarAbc,
		CodabarCx,
		Scode,
		Matrix2of5,
		Iata,
		KoreanPostal,
		Cca,
		Ccb,
		Ccc,
		Last
	}

	public enum ConnStates
	{
		Disconnected = 0,
		Connecting,
		Connected
	}

	public enum DeviceConnectionType 
	{
		Accessory,
		Tcpip,
		Bluetooth,
		Bluetoothle
	}

	[Flags]
	public enum BluetoothFilter
	{
		All = -1,
		Printers = 1,
		Pinpads = 2,
		BarcodeScanners = 4
	}

	public enum ScanModes 
	{
		SingleScan = 0,
		MultiScan,
		MotionDetect,
		SingleScanRelease,
		MultiScanNoDuplicates
	}

	public enum ButtonStates 
	{
		Disabled = 0,
		Enabled
	}

	public enum MsModes 
	{
		ProcessedCardData = 0,
		RawCardData = 1,
		ProcessedTrack2Data = 3
	}

	public enum TaModes
	{
		T1T2Combined = 0,
		T1Only,
		T2Only,
		T1T2Separate,
		PanOnly
	}

	public enum CertificateSlots
	{
		DataRoot = 0,
		DataIntermediate,
		DataKey,
		Params
	}

	[Flags]
	public enum FeatEmv2Kernels
	{
		Necomplus = 1,
		Datecs = 2,
		Universal = 4
	}

	[Flags]
	public enum Emv2Interfaces
	{
		Contact = 1,
		Contactless = 2,
		Magnetic = 4,
		MagneticManual = 8
	}

	public enum BtModes 
	{
		Default = 0,
		Extended,
		Iso15424
	}

	public enum UpdatePhases
	{
		Init = 0,
		Erase,
		Write,
		Finish,
		Completing
	}

	public enum MagCardEncryption
	{
		AlgAES256 = 0,
		AlgEhECC = 1,
		AlgEhAES256 = 2,
		AlgEhIdTech = 3,
		AlgEhIdTechAES128 = 0x0b,
		AlgEhMagtek = 4,
		AlgEhMagtekAES128 = 0x0c,
		AlgEh3DES = 5,
		AlgEhRSAOaep = 6,
		AlgPpad3DESCbc = 7,
		AlgEhVoltage = 8,
		AlgEhAES128 = 9,
		AlgPpadDukpt = 10,
		AlgTransArmor = 13
	}

	public enum AuthenticationKey
	{
		Default = 0x00,
		FlagLock = 1
	}

	public enum EncryptedHeadKey 
	{
		AES256Loading = 0x02,
		AES256Encryption1 = 0x01,
		AES256Encryption2 = 0x03,
		AES256Encryption3 = 0x04,
		AES128Encryption1 = 0x09,
		AES128Encryption2 = 0x0b,
		AES128Encryption4 = 0x0c,
		TMKAES = 0x10,
		DUKPTMaster1 = 0x20,
		DUKPTMaster2 = 0x21,
		DUKPTMaster3 = 0x22
	}

	public enum EncryptedHead
	{
		Real = 0,
		Emulated = 1
	}

	[Flags]
	public enum MsTrack
	{
		Tack1 = 0x01,
		Tack2 = 0x02,
		Tack3 = 0x04,
		TackJis = 0x20,
		All = Tack1 | Tack2 | Tack3 | TackJis
	}

	public enum CardTypeSourceTrack
	{
		Magnetic = 0,
		Contactless = 1,
		SmartCard = 2,
		Manual = 3
	}

	public enum KeyType
	{
		AES128_ECB = 0x04 >> 2,
		AES128_CBC = 0x08 >> 2,
		AES256_ECB = 0x0C >> 2,
		AES256_CBC = 0x10 >> 2,
		_3DES_ECB = 0x14 >> 2,
		_3DES_CBC = 0x18 >> 2,
		DUKPT_3DES_ECB = 0x1C >> 2,
		DUKPT_3DES_CBC = 0x20 >> 2,
		DUKPT_AES128_ECB = 0x24 >> 2,
		DUKPT_AES128_CBC = 0x28 >> 2
	}

	public enum TagsFormat
	{
		Datecs = 1
	}

	public enum PinEncryptionFormats 
	{
		PinFormatIso0 = 4,
		PinFormatIso1 = 5,
		PinFormatIso3 = 13
	}

	public enum IccTypes 
	{
		Icc = 0,
		Pin
	}

	public enum RsaVerifyKey 
	{
		Issuer = 0,
		Icc
	}

	public enum ScSlots 
	{
		Main = 0,
		Sam
	}

	public enum RfCardTypes
	{
		Unknown = 0,
		MifareMini,
		MifareClassic1k,
		MifareClassic4k,
		MifareUltralight,
		MifareUltralightC,
		Iso14443a,
		MifarePlus,
		Iso15693,
		MifareDesfire,
		Iso14443b,
		Felica,
		StSri,
		Payment,
		Picopass15693,
		Picopass14443b,
		Epassport
	}

	public enum FelicaSmarttagBateryStatuses 
	{
		Normal1 = 0,
		Normal2,
		Low1,
		Low2
	}

	public enum FelicaSmartTagDrawModes
	{
		WhiteBackground = 0,
		BlackBackground,
		KeepBackground,
		UseLayout
	}

	public enum Features 
	{
		Msr,
		Barcode,
		Bluetooth,
		BatteryCharging,
		ExternalSerialPort,
		RfReader,
		Printing,
		Smartcard,
		PinEntry,
		Emvl2Kernel,
		Vibration,
		Leds,
		Speaker,
		Hid,
		Sam,
		Max
	}

	public enum FeatureValue
	{
		Unsupported = 0,
		Supported = 1
	}

	public enum FeatPrintProtocols 
	{
		Escpos = 1,
		Zpl = 2
	}

	public enum FeatMsrs 
	{
		Plain = 1,
		PlainWithEncryption = 2,
		Encrypted = 4,
		Voltage = 8,
		EncryptedEmul = 16,
		TransArmor = 32
	}

	public enum FeatBarcodes 
	{
		Opticon = 1,
		Code = 2,
		Newland = 3,
		Intermec = 4,
		Motorola = 5
	}

	public enum FeatBluetooths 
	{
		Client = 1,
		Host = 2
	}

	public enum BatteryChips 
	{
		None = 0,
		Bq27421
	}

	public enum PrinterFont
	{
		Font12x24 = 0,
		Font9x16 = 1
	}

	public enum PrintBarcodeType
	{
		Upca = 0,
		Upce,
		Ean13,
		Ean8,
		Code39,
		Itf,
		Codabar,
		Code93,
		Code128,
		Pdf417,
		Code128auto,
		Ean128auto
	}

	public enum Pdf417Size
	{
		W2H4 = 0,
		W2H9,
		W2H15,
		W2H20,
		W7H4,
		W7H9,
		W7H15,
		W7H20,
		W12H4,
		W12H9,
		W12H15,
		W12H20,
		W20H4,
		W20H9,
		W20H15,
		W20H20
	}

	public enum Pdf417Eccl 
	{
		Pdf417Eccl0 = 0,
		Pdf417Eccl1,
		Pdf417Eccl2,
		Pdf417Eccl3,
		Pdf417Eccl4,
		Pdf417Eccl5,
		Pdf417Eccl6,
		Pdf417Eccl7,
		Pdf417Eccl8,
		Auto
	}

	public enum QrcodeEccl 
	{
		QrcodeEccl7 = 0,
		QrcodeEccl15,
		QrcodeEccl25,
		QrcodeEccl30
	}

	public enum QrcodeSize 
	{
		QrcodeSize1 = 1,
		QrcodeSize4 = 4,
		QrcodeSize6 = 6,
		QrcodeSize8 = 8,
		QrcodeSize10 = 10,
		QrcodeSize12 = 12,
		QrcodeSize14 = 14
	}

	public enum BarTextPosition
	{
		None = 0,
		Above = 1,
		Below = 2,
		Both = 3
	}

	public enum BarTextAlign
	{
		Left = 0,
		Center = 1,
		Right = 2,
		Justify = 3
	}

	public enum BarTextWordwrap
	{
		Wordwrap = 1
	}

	public enum BarTextRotate
	{
		Rotate0 = 0,
		Rotate90 = 1,
		Rotate180 = 2
	}

	public enum BarTextLinespace
	{
		Default = 0x22
	}

	public enum BarTextBlackmarkThreshold
	{
		Default = 0x68
	}

	[Flags]
	public enum Tableborders
	{
		Horizontal = 1,
		Vertical = 2,
		ColumnCompact = 4
	}

	public enum PageOrientation
	{
		HorizontalTopLeft = 0,
		VerticalBottomLeft = 1,
		HorizontalBottomRight = 2,
		VerticalTopRight = 3
	}

	public enum LogoMode
	{
		Normal = 0,
		DoubleWidth = 1,
		DoubleHeight = 2,
		DoubleWidthAndHeight = 3
	}

	public enum Animations
	{
		All = -1,
		InsertCard = 0,
		RemoveCard,
		Busy,
		Drop,
		InsertSmartcard,
		InsertMagneticCard
	}

	public enum Languages 
	{
		English = 0,
		Bulgarian,
		Spanish,
		Russian,
		Romanian,
		French,
		Finish,
		Swedish
	}

	public enum Codepages 
	{
		CpIso88591Latin1 = 0,
		CpIso88592Latin2,
		CpIso88593Latin3,
		CpIso88594Latin4,
		CpIso88595Cyrillic,
		CpIso88596Arabic,
		CpIso88597Greek,
		CpIso88598Hebrew,
		CpIso88599Latin5,
		CpIso885910Latin6
	}

	public enum Fonts 
	{
		Font6x8 = 0,
		Font8x16,
		Font4x6
	}

	public enum DTError
	{
		None = 0,
		General = -1,
		Create = -2,
		Open = -3,
		Close = -4,
		Busy = -5,
		Timeout = -6,
		NotSupported = -7,
		Memory = -8,
		Param = -9,
		IO = -10,
		CRC = -11,
		Flash = -12,
		EEProm = -13,
		Device = -14,
		NotImplemented = -15,
		NotExist = -16,
		InvalidCommand = -17,
		ObjectNotExist = -18,
		NoMoreItems = -19,
		Failed = -20,
		Invalid = -21,
		NotRegistered = -22,
		PermissionDenied = -23
	}

	public enum DTMifareError
	{
		Base = -10000,
		Timeout = Base - 1,
		Collision = Base - 2,
		Parity = Base - 3,
		Frame = Base - 4,
		CRC = Base - 5,
		FIFO = Base - 6,
		EEProm = Base - 7,
		Key = Base - 8,
		Generic = Base - 9,
		Authentication = Base - 10,
		Code = Base - 11,
		BitCount = Base - 12,
		Access = Base - 13,
		Value = Base - 14
	}

	public enum DTEMSRError
	{
		Base = -11000,
		InvalidCommand = Base - 0x01,
		NoPermission = Base - 0x02,
		Card = Base - 0x03,
		Syntax = Base - 0x04,
		NoResponse = Base - 0x05,
		NoData = Base - 0x06,
		InvalidLength = Base - 0x14,
		Tampered = Base - 0x15,
		InvalidSignature = Base - 0x16,
		Hardware = Base - 0x17
	}

	public enum DTPinpadError
	{
		Base = -16500,
		General = Base - 1,
		InvalidCommand = Base - 2,
		InvalidParameter = Base - 3,
		InvalidAddress = Base - 4,
		InvalidValue = Base - 5,
		InvalidLength = Base - 6,
		NoPermission = Base - 7,
		NoData = Base - 8,
		Timeout = Base - 9,
		InvalidKeyNumber = Base - 10,
		InvalidAttributes = Base - 11,
		InvalidDevice = Base - 12,
		NotSupported = Base - 13,
		LimitExceeded = Base - 14,
		Flash = Base - 15,
		Hardware = Base - 16,
		InvalidCCR = Base - 17,
		Cancelled = Base - 18,
		InvalidSignature = Base - 19,
		InvalidHeader = Base - 20,
		InvalidPassword = Base - 21,
		InvalidKeyFormat = Base - 22,
		SCR = Base - 23,
		Hal = Base - 24,
		InvalidKey = Base - 25,
		InvalidPin = Base - 26,
		InvalidRemainder = Base - 27,
		NotInitialized = Base - 28,
		LimitReached = Base - 29,
		InvalidSequence = Base - 30,
		NotPermitted = Base - 31,
		NoTMK = Base - 32,
		WrongKey = Base - 33,
		DuplicatedKey = Base - 34,
		KeyboardGeneral = Base - 35,
		KeyboardNotCalibrated = Base - 36,
		KeyboardFailure = Base - 37
	}

	public enum ScreenColorModes 
	{
		ColorModeBw = 0
	}

	public enum Parity
	{
		None = 0,
		Even = 1,
		Odd = 2
	}

	public enum DataBits
	{
		Seven = 1,
		Eight = 0
	}

	public enum StopBits
	{
		One = 0,
		Two = 1
	}

	public enum FlowControl
	{
		None = 0,
		RtsCts = 1,
		DtrDsr = 2,
		XonXoff = 3
	}

	public enum LNEMSRError
	{
		Base = -11000,
		InvalidCommand = Base - 0x01,
		NoPermission = Base - 0x02,
		Card = Base - 0x03,
		Syntax = Base - 0x04,
		NoResponse = Base - 0x05,
		NoData = Base - 0x06,
		InvalidLength = Base - 0x14,
		Tampered = Base - 0x15,
		InvalidSignature = Base - 0x16,
		Hardware = Base - 0x17
	}

	public enum VoltageEncryption
	{
		Full = 0,
		Spe = 1
	}

	public enum RfChannel
	{
		Iso1443A = 0x01,
		Iso1443B = 0x02,
		Iso15693 = 0x03,
		Felica = 0x04,
		Stsri = 0x05,
		PicopassIso15 = 0x06
	}

	[Flags]
	public enum RFCardSupport
	{
		TypeA = 0x0001,
		TypeB = 0x0002,
		Felica = 0x0004,
		Nfc = 0x0008,
		Jewel = 0x0010,
		Iso15 = 0x0020,
		Stsri = 0x0040,
		PicoPassIso14 = 0x0080,
		PicoPassIso15 = 0x0100,
	}

	public enum EmvTransactionResult
	{
		Approved = 0x0800,
		Declined = 0x0400,
		TryAnotherInterface = 0x0100,
		TryAgain = 0x1000,
		EndApplication = 0x0000
	}

	public enum CvmResult
	{
		OnlinePin = 0x01,
		CodeVerified = 0x02,
		ObtainSignature = 0x03,
		NoCvm = 0x04,
		OfflinePin = 0x08
	}

	public enum EmvUiCodes 
	{
		StatusNotReady = 0x00,
		StatusIdle = 0x01,
		StatusReadyToRead = 0x02,
		StatusProcessing = 0x03,
		StatusCardReadSuccess = 0x04,
		StatusErrorProcessing = 0x05,
		NotWorking = 0x0000,
		Approved = 0x0003,
		Declined = 0x0007,
		PleaseEnterPin = 0x0009,
		ErrorProcessing = 0x000F,
		RemoveCard = 0x0010,
		Idle = 0x0014,
		PresentCard = 0x0015,
		Processing = 0x0016,
		CardReadOkRemove = 0x0017,
		TryOtherInterface = 0x0018,
		CardCollision = 0x0019,
		SignApproved = 0x001A,
		OnlineAuthorisation = 0x001B,
		TryOtherCard = 0x001C,
		InsertCard = 0x001D,
		ClearDisplay = 0x001E,
		SeePhone = 0x0020,
		PresentcCardAgain = 0x0021,
		SelectApplication = 0x8001,
		ManualEntry = 0x8002,
		PromptEnterPin = 0x8003,
		PromptWrongPin = 0x8004,
		PromptPinLastAttempt = 0x8005,
		Na = 0x00FF,
	}

	public enum TvrBits
	{
		DefaultTdolUsed = 0x0508,
		IssuerAuthFailed = 0x0507,
		ScriptFailBeforeAc = 0x0506,
		ScriptFailAfterAc = 0x0505,
		TerminalLimitExceeded = 0x0408,
		LowerOffLimitExceeded = 0x0407,
		UpperOffLimitExceeded = 0x0406,
		RandomSelectionOnline = 0x0405,
		MerchantForceOnline = 0x0404,
		CardholderVerifFailure = 0x0308,
		VerifMethodUnknown = 0x0307,
		PinLimitExceeded = 0x0306,
		PinAskedPinpadFailured = 0x0305,
		PinAskedButNotEntered = 0x0304,
		OnlinePinEntered = 0x0303,
		SoftwareVersions = 0x0208,
		ApplicationExpired = 0x0207,
		ApplicationNotEffective = 0x0206,
		ReqServiceNotAllowed = 0x0205,
		NewCard = 0x0204,
		OffdataAuthNotDone = 0x0108,
		StaticAuthFailed = 0x0107,
		DataNotFound = 0x0106,
		CardInHotList = 0x0105,
		DynamicAuthFailed = 0x0104,
		CombinedDdaFailed = 0x0103
	}

	public enum TsiBits
	{
		OffdataAuthDone = 0x0108,
		CardholderVerifDone = 0x0107,
		CardRiskDone = 0x0106,
		IssuerAuthDone = 0x0105,
		TerminalRiskDone = 0x0104,
		ScriptProcessDone = 0x0103
	}

	public enum AppSelectionMethods 
	{
		Pse = 0,
		Aidlist
	}

	public enum AppMatchCriterias
	{
		Full = 1,
		PartialVisa,
		PartialEuropay
	}

	public enum AuthResults 
	{
		ResultSuccess = 1,
		ResultFailure,
		FailPinEntryNotDone,
		FailUserCancellation
	}

	public enum BypassModes 
	{
		CurrentMethodMode = 0,
		AllMethodsMode
	}

	public enum CertificateAcTypes 
	{
		Aac = 0,
		Tc,
		Arqc
	}

	public enum CardRiskTypes 
	{
		Cdol1 = 1,
		Cdol2
	}

	public enum TagTypes 
	{
		Binary = 0,
		Bcd,
		String
	}

	public enum EmvTags
	{
		Pan = 0x5A,
		Cdol1 = 0x8C,
		Cdol2 = 0x8D,
		CvmList = 0x8E,
		Tdol = 0x97,
		IssuerPkCertificate = 0x90,
		SignedStaAppDat = 0x93,
		IssuerPkRemainder = 0x92,
		CaPkIndex = 0x8F,
		CardholderName = 0x5F20,
		ServiceCode = 0x5F30,
		CardholderNameExten = 0x9F0B,
		ExpiryDate = 0x5F24,
		EffectiveDate = 0x5F25,
		IssuerCountryCode = 0x5F28,
		IssuerCountryCodeA2 = 0x5F55,
		IssuerCountryCodeA3 = 0x5F56,
		PanSequenceNumber = 0x5F34,
		AppDiscretionDat = 0x9F05,
		AppUsageControl = 0x9F07,
		IccAppVersionNumber = 0x9F08,
		IssuerActionDefault = 0x9F0D,
		IssuerActionDenial = 0x9F0E,
		IssuerActionOnline = 0x9F0F,
		ApplRefCurrency = 0x9F3B,
		ApplCurrencyCode = 0x9F42,
		ApplRefCurrencyExp = 0x9F43,
		ApplCurrencyExp = 0x9F44,
		IccPkCertificate = 0x9F46,
		IccPinPkCertificate = 0x9F2D,
		IccPkExp = 0x9F47,
		IccPinPkExp = 0x9F2E,
		IccPkRemainder = 0x9F48,
		IccPinPkRemainder = 0x9F2F,
		StaDatAuthTagList = 0x9F4A,
		Ddol = 0x9F49,
		IssuerPkExp = 0x9F32,
		LowConsecOfflineLimit = 0x9F14,
		UppConsecOfflineLimit = 0x9F23,
		Track2DiscretionDat = 0x9F20,
		Track1DiscretionDat = 0x9F1F,
		Track2EquivalentData = 0x57,
		UnpredictableNumber = 0x9F37,
		AcquirerIdentifier = 0x9F01,
		AddTermCapabilities = 0x9F40,
		AmountAuthorisedBinary = 0x81,
		AmountAuthorisedNum = 0x9F02,
		AmountOtherBinary = 0x9F04,
		AmountOtherNum = 0x9F03,
		AmountRefCurr = 0x9F3A,
		AppCryptogram = 0x9F26,
		Afl = 0x94,
		IccAid = 0x4F,
		TermAid = 0x9F06,
		Aip = 0x82,
		AppLabel = 0x50,
		AppPreferredName = 0x9F12,
		AppPriorityIndicator = 0x87,
		Atc = 0x9F36,
		AppVersionNumber = 0x9F09,
		AuthCode = 0x89,
		AuthRespCode = 0x8A,
		ChVerifyMethodResult = 0x9F34,
		CaPublicKeyIndex = 0x9F22,
		CryptInfoData = 0x9F27,
		DatAuthCode = 0x9F45,
		IccDynNumber = 0x9F4C,
		SerialNumber = 0x9F1E,
		IssuerAppDat = 0x9F10,
		IssuerAuthDat = 0x91,
		IssuerCodeIndex = 0x9F11,
		LanguagePreference = 0x5F2D,
		Latc = 0x9F13,
		MerchantCategoryCode = 0x9F15,
		MerchantIdentifier = 0x9F16,
		PinTryCounter = 0x9F17,
		PosEntryCode = 0x9F39,
		Pdol = 0x9F38,
		TerminalCapabilities = 0x9F33,
		TerminalCountryCode = 0x9F1A,
		TerminalFloorLimit = 0x9F1B,
		TerminalId = 0x9F1C,
		TerminalRiskDat = 0x9F1D,
		TerminalType = 0x9F35,
		Tvr = 0x95,
		TransactionCurrCode = 0x5F2A,
		TransactionCurrExp = 0x5F36,
		TransactionDate = 0x9A,
		TransactionRefCurrCode = 0x9F3C,
		TransactionRefCurrExp = 0x9F3D,
		TransactionSeqCounter = 0x9F41,
		Tsi = 0x9B,
		TransactionTime = 0x9F21,
		TransactionType = 0x9C,
		SignedDynAppDat = 0x9F4B,
		TcHashValue = 0x98,
		AccountType = 0x5F37,
		BankIdentifierCode = 0x5F54,
		Iban = 0x5F53,
		IssuerIdentificationNumber = 0x42,
		IssuerUrl = 0x5F50,
		LogEntry = 0x9F4D,
		TransactionCategoryCode = 0x9F53,
		RiskAmount = 0xDF02,
		TermActionDefault = 0xDF03,
		TermActionDenial = 0xDF04,
		TermActionOnline = 0xDF05,
		ThresholdValue = 0xDF07,
		TargetPercentage = 0xDF08,
		MaxTargetPercentage = 0xDF09,
		DefaultDdol = 0xDF15,
		DefaultTdol = 0xDF18,
		FloorLimitCurrency = 0xDF19,
		OffAuthDat = 0xDF23,
		IssuerScripts = 0xDF24,
		IssuerScriptsResult = 0xDF25
	}

	public enum EmvStatusCodes
	{
		Success = 0,
		ListAvailable = 1,
		ApplicationAvailable = 2,
		NoCommonApplication = 3,
		EasyEntryApp = 4,
		AmountNeeded = 5,
		ResultNeeded = 6,
		AuthCompleted = 7,
		AuthNotDone = 8,
		OfflinePinPlain = 9,
		OnlinePin = 10,
		OfflinePinCiphered = 11,
		BlockedApplication = 12,
		TransactionOnline = 13,
		TransactionApproved = 14,
		TransactionDenied = 15,
		CdaFailed = 16,
		InvalidPin = 17,
		InvalidPinLastAttempt = 18,
		Failure = 50,
		NoDataFound = 51,
		SystemError = 52,
		DataFormatError = 53,
		InvalidAtr = 54,
		AbortTransaction = 55,
		ApplicationNotFound = 56,
		InvalidApplication = 57,
		ErrorInApplication = 58,
		CardBlocked = 59,
		NoScriptLoaded = 60,
		TagNotFound = 61,
		InvalidTag = 62,
		InvalidLength = 63,
		InvalidHash = 64,
		InvalidKey = 65,
		NoMoreKeys = 66,
		ErrorAcProcess = 67,
		ErrorAcDenied = 68,
		NoCurrentMethod = 69,
		ResultAlreadyLoaded = 70,
		LastEmvKernelErrCode = 70,
		InvalidRemainder = 80,
		InvalidHeader = 81,
		InvalidFooter = 82,
		InvalidFormat = 83,
		InvalidCertificate = 84,
		InvalidSignature = 85
	}

	public enum Channel
	{
		Prn = 1,
		SmartCard = 2,
		Gprs = 5,
		Encmsr = 14,
		Mifare = 16,
		Zpl = 50
	}

	public enum SdkDebugSource
	{
		ConnectedDevice = 0,
		Sdk = 1
	}
}

